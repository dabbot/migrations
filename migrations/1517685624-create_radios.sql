-- Migration: create_radios
-- Created at: 2018-02-03 14:20:24
-- ====  UP  ====

begin;

create table radios (
    id serial primary key,
    name varchar(128) not null,
    locale varchar(8) not null,
    url varchar(128) unique not null,
    category_id integer not null references radio_categories(id),
    genre_id integer not null references radio_genres(id),
    track_data varchar(1024),

    created_at timestamp not null default now(),
    updated_at timestamp not null default now()
);

commit;

-- ==== DOWN ====

begin;

drop table radios;

commit;
