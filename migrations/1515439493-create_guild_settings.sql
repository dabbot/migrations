-- Migration: create_guild_settings
-- Created at: 2018-01-08 11:24:53
-- ====  UP  ====

begin;

create table guild_settings (
    id serial primary key,
    guild_id bigint not null unique,
    prefix varchar(10)
);

commit;

-- ==== DOWN ====

begin;

drop table guild_settings;

commit;
