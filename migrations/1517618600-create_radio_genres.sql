-- Migration: create_radio_genres
-- Created at: 2018-02-02 19:43:20
-- ====  UP  ====

begin;

create table radio_genres (
    id serial primary key,
    name varchar(32) not null unique
);

commit;

-- ==== DOWN ====

begin;

drop table radio_genres;

commit;
