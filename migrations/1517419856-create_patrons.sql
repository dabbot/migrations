-- Migration: create_patrons
-- Created at: 2018-01-31 09:30:56
-- ====  UP  ====

begin;

create type supporter_level_type as enum (
    'supporter',
    'super supporter',
    'super duper supporter'
);

create table patrons (
    user_id bigint primary key not null,
    role_ids bigint[] not null default '{}'
);

commit;

-- ==== DOWN ====

begin;

drop table patrons;
drop type supporter_level_type;

commit;
