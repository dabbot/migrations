-- Migration: create_radio_categories
-- Created at: 2018-02-02 19:43:16
-- ====  UP  ====

begin;

create table radio_categories (
    id serial primary key,
    name varchar(32) not null unique
);

commit;

-- ==== DOWN ====

begin;

drop table radio_categories;

commit;
