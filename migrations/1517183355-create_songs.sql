-- Migration: create_songs
-- Created at: 2018-01-28 18:49:16
-- ====  UP  ====

begin;

create table songs (
    song_id serial primary key,
    track_data text not null unique
);

commit;

-- ==== DOWN ====

begin;

drop table songs;

commit;
