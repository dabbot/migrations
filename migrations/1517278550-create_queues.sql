-- Migration: create_queues
-- Created at: 2018-01-29 18:15:50
-- ====  UP  ====

begin;

create table queues (
    guild_id bigint primary key,
    song_ids int[] not null default '{}'
);

commit;

-- ==== DOWN ====

begin;

drop table queues;

commit;
